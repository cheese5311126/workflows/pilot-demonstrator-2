# High-resolution seismic inversion and tomography

**PD leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/CNRS.png?inline=false" width="50"> 

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/csic.png?inline=false" width="80">,<img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/bsc.png?inline=false" width="120">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="35">

**Codes:** 

**Derived Simulation Cases:** [SC2.1](https://gitlab.com/cheese5311126/simulation-cases/sc2.1)

## Description

High-resolution subsurface imaging entails high-frequency computations up to 20Hz to achieve a
resolution of a few tens of meters. Given the very strong heterogeneity and anisotropy of the upper crust and the strong
attenuation effect at the considered frequencies, the complete anisotropic tensor and the material attenuation need to be
inverted, requiring second order optimization methods such as Newton or Gauss-Newton to perform multi-parameter
inversions in a more robust way.

## Objective

The objective of this PD is to define a complete and flexible workflow for seismic inversion in conjunction with
other methods such as gravimetry (density is an important physical parameter that is not well-resolved yet by seismic
tomography). This implies: 

(1) Joint inversion with gravimetric data defining a multi-objective functional that contains both the seismic and gravity data. 

(2) Computation of the initial guess model using methods based on the Eikonal equation to make a tomography of the first arrival times. 

(3) Add new parameters in the inversion to account for upper crust heterogeneity, anisotropy and attenuation. 

(4) Use a 2nd-order optimization method (gradient+Hessian) to address the shortcomings of the classical adjoint method. 

These 2nd-order methods are not used yet in 3D and in time-domain solvers because of the 10-100 times cost increase compared to the classical (gradient) method. This PD is an evolution of the ChEESE-1P PD9 (seismic tomography, final TRL 6) to focus on the local scale, i.e. on areas of a few kilometers to a few tens of kilometers using local seismicity/gravity data. Challenges at-scale concerning the local seismicity include locating the hypocenters, inverting the moment tensor, and computation of the source time function. For these reasons the start TRL has been lowered to 4.